var app = require("express")()
var server = app.listen(8888)
var io = require('socket.io')(server,{})
let userlist=[],
  messages=[],
  storage=[],
  messageLimit=20,
  allowAccountCreation=true
app.get('/',(x,res)=>{res.sendFile(__dirname+'/index.html')})
app.get('/format.js',(x,res)=>{res.sendFile(__dirname+'/format.js')})
app.get('/style.css',(x,res)=>{res.sendFile(__dirname+'/style.css')})
io.on('connect',socket=>{
  console.log(socket.id)
  socket.on('auth',e=>{authClient(socket,e)})
  socket.on('relayMessage',e=>{relayMessage(socket,e)})
  socket.on('loadMoreMessages',e=>{loadMoreMessages(socket,e)})
  socket.on('disconnect',e=>{console.log('disconnect',socket.id)})
  socket.on('iamalive',_=>{socket.emit('iamalsoalive')})
})

const findInUserlist=username=>{
  for(let i=0;i<userlist.length;i++){
    if(userlist[i].username==username){
      return i+1
    }
  }
  return 0
}

const authClient=(socket,authStuff)=>{
  console.log('auth attempt: username ',authStuff.username,' hashed passcode ',authStuff.hashedPasscode)
  user = findInUserlist(authStuff.username)
  if(user){
    user--
    if(userlist[user].hashedPasscode == authStuff.hashedPasscode){
      console.log('auth accepted ',authStuff.username)
        userlist[user].sessionToken = authStuff.hashedPasscode-socket.id.hashCode()
      socket.authed=1
        socket.join("authed")
        socket.emit("authAccepted")
      updateMessages(socket)
        return
      }else{
        console.log('auth failed ',authStuff.username)
      socket.authed=0
        socket.emit("authFailed")
        return
      }
    }else if(allowAccountCreation){
      console.log('new user ',authStuff.username)
        userlist.push({
            username:authStuff.username,displayColor:pickRandomColorFromPallete(),hashedPasscode:authStuff.hashedPasscode,
            sessionToken:authStuff.hashedPasscode-socket.id.hashCode()
        })
        socket.authed=1
        socket.join("authed")
        socket.emit("authAccepted")
    updateMessages(socket)
        return
    }
}

const relayMessage=(socket,message)=>{
  user = findInUserlist(message.name)
    if(user){
        user--
        if(userlist[user].sessionToken + socket.id.hashCode() == userlist[user].hashedPasscode){
          console.log("message sent ",message.name, message.message)
          messages.push({
            message:message.message,name:message.name,color:userlist[user].displayColor,timestamp:message.timestamp,id:message.id
          })
          if(messages.length>messageLimit){
            storage.push(messages)
            messages=[]
          }
            io.to("authed").emit("relayedMessage",{
                message:message.message,name:message.name,color:userlist[user].displayColor,timestamp:message.timestamp,id:message.id
            })
        }
    }
}

const updateMessages=socket=>{
  for(let i=0;i<messages.length;i++){
    socket.emit("relayedMessage",{message:messages[i].message,name:messages[i].name,color:messages[i].color,timestamp:messages[i].timestamp,id:messages[i].id})
  }
  if(storage.length){
    for(let i=0;i<storage[storage.length-1].length;i++){
        socket.emit("relayedMessage",{
          message:storage[storage.length-1][i].message,name:storage[storage.length-1][i].name,color:storage[storage.length-1][i].color,
          timestamp:storage[storage.length-1][i].timestamp,id:storage[storage.length-1][i].id
        })
      }
  }
}

const loadMoreMessages=(socket,e)=>{
  let n=e
  if(socket.authed){
    //do stuff
    for(let i=0;i<messages.length;i++){
      socket.emit("relayedMessage",{message:messages[i].message,name:messages[i].name,color:messages[i].color,timestamp:messages[i].timestamp,id:messages[i].id})
    }
    //history thing
    let storageSection=Math.floor(Math.max(0,e-3)/messageLimit)-1
    console.log("unparsed",storageSection)
    storageSection=storage.length-1-storageSection
    if(storageSection<storage.length&&storageSection>-1){
      console.log("request pull storage",storageSection)
      for(let i=0;i<storage[storageSection].length;i++){
        socket.emit("relayedMessage",{
          message:storage[storageSection][i].message,name:storage[storageSection][i].name,color:storage[storageSection][i].color,
          timestamp:storage[storageSection][i].timestamp,id:storage[storageSection][i].id
        })
      }
    }
  }
}

const pickRandomColorFromPallete=_=>{
  return pallete[Math.floor(Math.random()*pallete.length)]
}
const pallete=[
  "#F00",
  "#0A0",
  "#66F",
  "#FF0",
  "#9030ee"
]
String.prototype.hashCode=function(){let h=0;if(this.length==0){return h}for(let i=0;i<this.length;i++){let c=this.charCodeAt(i);h=((h<<5)-h)+c;h=h&h}return h} //replace with better string->int hash later