//format incoming markdown text to html
format=m=>{
  m=m.replaceAll("<","<​").replaceAll(">","​>")
  return m
}
let noextras = /[^A-Za-z0-9]/g
formatName=m=>{
  return m.replace(noextras,"")
}